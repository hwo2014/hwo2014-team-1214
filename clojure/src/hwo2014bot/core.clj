(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.game :refer :all])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn print-event [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "SOMEONE CRASHED")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn log [msg]
  (println (str "message=" msg)))

(defn game-loop [channel model]
  (let [msg (read-message channel)
        new-model (handle-msg msg model)]
    (print-event msg)
    (log msg)
    (let [update (first (:actions new-model))]
      (log update)
      (send-message channel update)
      (recur channel new-model))))

;Run with "lein run senna.helloworldopen.com 8091 Insultants PoFpmajuUy5viQ <trackname" keimola(default)/usa/germany
(defn -main[& [host port botname botkey track]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "createRace" :data {:botId { :name botname :key botkey} :trackName track :password "asdfnagdn" :carCount 1}})
    (game-loop channel {})))
