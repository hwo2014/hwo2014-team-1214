(ns hwo2014bot.game)

; utils

(defn pieces [model]
  (get-in model [:race :track :pieces]))

(defn my-car-name [model]
  (get-in model [:car :name]))

(defn lane-radius [lane piece]
  (let [angle (get piece :angle 0)]
    (if (= angle 0)
      0
      (if (> angle 0) 
        (- (get piece :radius 0) (:distanceFromCenter lane))
        (+ (get piece :radius 0) (:distanceFromCenter lane))))))

(defn car [msg car-name]
  (first (filter #(= (-> % :id :name) car-name) (:data msg))))

(defn car-piece-index [car]
  (get-in car [:piecePosition :pieceIndex]))

(defn piece-at [pieces i]
  (nth pieces i))

; Imitates a cyclic data structure for the race pieces (as in laps)
; offset positive for pieces ahead forward, negative for pieces behind
(defn piece-at-offset [pieces i offset]
  ; TODO, anything after the finish line == imitate as straight for maximum thrust
  (let [size (count pieces)
        piece (piece-at pieces (rem (+ i offset) size ))]
    piece))

; actions

(defn throttle [model [throttle]]
  {:pre [(>= throttle 0) (<= throttle 1)]}
  {:msgType "throttle" :data throttle})

(defn switch-lane [model [direction]]
  {:pre [(or (= direction "Left") (= direction "Right"))]}
  {:msgType "switchLane" :data direction})

(defn ping [model [_]]
  {:msgType "ping" :data "ping"})

(defn action [model f & args]
  "Use this with any of the actions you can make (ping, swithch-lane or throttle)
   Usage (-> model (action throttle 0.5))

   Adds this action to the list of actions as latest action."
  (update-in model [:actions] conj (f model args)))

; -- Server message handling per type

(defmulti handle-msg :msgType)

; {"msgType": "carPositions", "data": [
;   {
;     "id": {
;       "name": "Schumacher",
;       "color": "red"
;     },
;     "angle": 0.0,
;     "piecePosition": {
;       "pieceIndex": 0,
;       "inPieceDistance": 0.0,
;       "lane": {
;         "startLaneIndex": 0,
;         "endLaneIndex": 0
;       },
;       "lap": 0
;     }
;   },
;   {
;     "id": {
;       "name": "Rosberg",
;       "color": "blue"
;     },
;     "angle": 45.0,
;     "piecePosition": {
;       "pieceIndex": 0,
;       "inPieceDistance": 20.0,
;       "lane": {
;         "startLaneIndex": 1,
;         "endLaneIndex": 1
;       },
;       "lap": 0
;     }
;   }
; ], "gameId": "OIUHGERJWEOI", "gameTick": 0}
(defmethod handle-msg "carPositions" [msg model]
  (let [pieces (pieces model)
        my-name (my-car-name model)
        my-car (car msg my-name)
        i (car-piece-index my-car)
        first-piece (piece-at-offset pieces i 0)
        second-piece (piece-at-offset pieces i 1)
        third-piece (piece-at-offset pieces i 2)
        fourth-piece (piece-at-offset pieces i 3)
        first-angle (get first-piece :angle 0)
        second-angle (get second-piece :angle 0)
        third-angle (get third-piece :angle 0)
        fourth-angle (get fourth-piece :angle 0)
        car-angle (get my-car :angle 0)
        throttle-value (Math/max 0.2 (+
                                       (if (< (Math/abs first-angle) 23) 1.0 0.6)
                                       (if (< (Math/abs car-angle) 15) 0.1 -0.2)
                                       (if (< (Math/abs car-angle) 25) 0.0 -0.5)
                                       (if (and (< (Math/abs first-angle) 23) (> (Math/abs second-angle) 23)) -0.5 0)
                                       (if (and (< (Math/abs first-angle) 23) (> (Math/abs third-angle) 23)) -0.4 0)
                                       (if (and (< (Math/abs first-angle) 23) (> (Math/abs fourth-angle) 23)) -0.1 0)))]
    (println (str "mycar=" my-car))
    (println (str "road ahead=" first-piece ", " second-piece "," third-piece ", " fourth-piece))
    (println (str "next throttle=" throttle-value))
    (-> model 
      (action throttle (Math/min throttle-value 1.0)))))


; Example
; {"msgType": "yourCar", "data": {
;   "name": "Schumacher",
;   "color": "red"
; }}
(defmethod handle-msg "yourCar" [msg model]
  (->
    model
    (assoc :car (:data msg))
    (action ping)))

; Example
; {"msgType": "gameInit", "data": {
;   "race": {
;     "track": {
;       "id": "indianapolis",
;       "name": "Indianapolis",
;       "pieces": [
;         {
;           "length": 100.0
;         },
;         {
;           "length": 100.0,
;           "switch": true
;         },
;         {
;           "radius": 200,
;           "angle": 22.5
;         }
;       ],
;       "lanes": [
;         {
;           "distanceFromCenter": -20,
;           "index": 0
;         },
;         {
;           "distanceFromCenter": 0,
;           "index": 1
;         },
;         {
;           "distanceFromCenter": 20,
;           "index": 2
;         }
;       ],
;       "startingPoint": {
;         "position": {
;           "x": -340.0,
;           "y": -96.0
;         },
;         "angle": 90.0
;       }
;     },
;     "cars": [
;       {
;         "id": {
;           "name": "Schumacher",
;           "color": "red"
;         },
;         "dimensions": {
;           "length": 40.0,
;           "width": 20.0,
;           "guideFlagPosition": 10.0
;         }
;       },
;       {
;         "id": {
;           "name": "Rosberg",
;           "color": "blue"
;         },
;         "dimensions": {
;           "length": 40.0,
;           "width": 20.0,
;           "guideFlagPosition": 10.0
;         }
;       }
;     ],
;     "raceSession": {
;       "laps": 3,
;       "maxLapTimeMs": 30000,
;       "quickRace": true
;     }
;   }
; }}
(defmethod handle-msg "gameInit" [msg model]
  (->
    model
    (assoc :race (:race (:data msg))) ; fixme: get :race
    (action ping)))

; {"msgType": "gameStart", "data": null}
(defmethod handle-msg "gameStart" [msg model]
  (->
    model
    (assoc :game-started true)
    (action ping)))

; {"msgType": "gameEnd", "data": {
;   "results": [
;     {
;       "car": {
;         "name": "Schumacher",
;         "color": "red"
;       },
;       "result": {
;         "laps": 3,
;         "ticks": 9999,
;         "millis": 45245
;       }
;     },
;     {
;       "car": {
;         "name": "Rosberg",
;         "color": "blue"
;       },
;       "result": {}
;     }
;   ],
;   "bestLaps": [
;     {
;       "car": {
;         "name": "Schumacher",
;         "color": "red"
;       },
;       "result": {
;         "lap": 2,
;         "ticks": 3333,
;         "millis": 20000
;       }
;     },
;     {
;       "car": {
;         "name": "Rosberg",
;         "color": "blue"
;       },
;       "result": {}
;     }
;   ]
; }}
(defmethod handle-msg "gameEnd" [msg model]
  (->
    model
    (assoc :game-started false)
    (action ping)))

; {"msgType": "tournamentEnd", "data": null}
(defmethod handle-msg "tournamentEnd" [msg model]
  (->
    model
    (assoc :game-started false)
    (action ping)))

; {"msgType": "crash", "data": {
;   "name": "Rosberg",
;   "color": "blue"
; }, "gameId": "OIUHGERJWEOI", "gameTick": 3}
(defmethod handle-msg "crash" [msg model]
  (-> model (action ping)))

; {"msgType": "spawn", "data": {
;   "name": "Rosberg",
;   "color": "blue"
; }, "gameId": "OIUHGERJWEOI", "gameTick": 150}
(defmethod handle-msg "spawn" [msg model]
  (-> model (action ping)))

; {"msgType": "lapFinished", "data": {
;   "car": {
;     "name": "Schumacher",
;     "color": "red"
;   },
;   "lapTime": {
;     "lap": 1,
;     "ticks": 666,
;     "millis": 6660
;   },
;   "raceTime": {
;     "laps": 1,
;     "ticks": 666,
;     "millis": 6660
;   },
;   "ranking": {
;     "overall": 1,
;     "fastestLap": 1
;   }
; }, "gameId": "OIUHGERJWEOI", "gameTick": 300}
(defmethod handle-msg "lapFinished" [msg model]
  (-> model (action ping)))

; {"msgType": "dnf", "data": {
;   "car": {
;     "name": "Rosberg",
;     "color": "blue"
;   },
;   "reason": "disconnected"
; }, "gameId": "OIUHGERJWEOI", "gameTick": 650}
(defmethod handle-msg "dfn" [msg model]
  (-> model (action ping)))

; {"msgType": "finish", "data": {
;   "name": "Schumacher",
;   "color": "red"
; }, "gameId": "OIUHGERJWEOI", "gameTick": 2345}
(defmethod handle-msg "finish" [msg model]
  (-> model (action ping)))

; Server sends something unexpected
(defmethod handle-msg :default [msg model]
  (-> model (action ping)))