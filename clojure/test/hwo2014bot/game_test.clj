(ns hwo2014bot.game-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.game :refer :all]))


(deftest action-test-parameterless
  (testing "Parameterless action should give some response"
    ; Ping happens to be parameterles so lets just test that
    (is (action {} ping))))

(deftest action-test
  (testing "Throttle action should just work"
    (is (get (action {} throttle 0.5) :msgType) "throttle")
    (is (get (action {} throttle 0.5) :data) 0.5)))

(deftest action-test
  (testing "Throttle action should not work with values out of range"
    (is (thrown? AssertionError (action {} throttle -1)))
    (is (thrown? AssertionError (action {} throttle -0.0000000001)))
    (is (thrown? AssertionError (action {} throttle 1.0000000001)))))